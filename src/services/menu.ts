import http from "@/services/axios";
import type Menu from "@/type/Menu";

function getMenu() {
  return http.get("/menu");
}

function saveMenu(menu: Menu & { files: File[] }) {
  const formData = new FormData();
  formData.append("Menu_name", menu.Menu_name);
  formData.append("Menu_price", `${menu.Menu_price}`);
  formData.append("file", menu.files[0]);
  return http.post("/menu", formData, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}

export default { getMenu, saveMenu };
