import type Stock from "@/type/Stock";
import http from "./axios";
function getStocks() {
  return http.get("/stocks");
}
function saveStock(stock: Stock) {
  return http.post("/stocks", stock);
}
function updateStock(id: number, stock: Stock) {
  return http.patch(`/stocks/${id}`, stock);
}
function deleteStock(id: number) {
  return http.delete(`/stocks/${id}`);
}
export default { getStocks, saveStock, updateStock, deleteStock };
