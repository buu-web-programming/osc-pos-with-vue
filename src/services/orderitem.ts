import type Stock from "@/type/Stock";
import http from "./axios";
import type Orderitem from "@/type/orderitem";
function getOrderitem() {
  return http.get("/orderitem");
}
function saveOrderitem(orderitem: Orderitem) {
  return http.post("/orderitem", orderitem);
}
function updateOrderitem(id: number, orderitem: Orderitem) {
  return http.patch(`/orderitem/${id}`, orderitem);
}
function deleteOrderitem(id: number) {
  return http.delete(`/orderitem/${id}`);
}
export default { getOrderitem,saveOrderitem,updateOrderitem,deleteOrderitem };
