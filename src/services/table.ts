import type Table from "@/type/Table";
import http from "./axios";
import axios from "axios";
function getTable() {
  return http.get("/table");
}
async function createTable(id: number, table: Table) {
  await axios.post("http://localhost:3000/order", {
    Order_total: 0,
    Table_id: id,
    user: 1,
    Order_status: "waiting",
  });
  return http.post(`/table/${id}`, table);
}
function deleteStock(id: number, table: Table) {
  return http.patch(`/table/${id}`, table);
}
export default { createTable, getTable, deleteStock };
