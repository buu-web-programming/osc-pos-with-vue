import type NewUser from "@/type/NewUser";
import type User from "@/type/User";
import http from "./axios";
function getUsers() {
  return http.get<User[]>("/users");
}
function getUserById(id: number) {
  return http.get<User>(`/users/${id}`);
}
function loginUser(username: string, password: string) {
  return http.post<boolean>("/login/", { username, password });
}
function createUser(user: NewUser) {
  return http.post<NewUser>("/users", user);
}
function deleteUser(id: number) {
  return http.delete<User>(`/users/${id}`);
}
function updateUser(user: User) {
  return http.patch<User>(`/users/${user.id}`, user);
}
export default {
  createUser,
  deleteUser,
  getUserById,
  updateUser,
  getUsers,
  loginUser,
};
