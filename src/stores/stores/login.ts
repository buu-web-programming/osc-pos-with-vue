import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessageStore } from "./message";

export const useLoginStore = defineStore("login", () => {
  const userStore = useUserStore();
  const messageStore = useMessageStore();
  const loginName = ref("");
  const isLogin = computed(() => {
    return loginName.value !== "";
  });
  const login = async (userName: string, password: string) => {
    const login = await userStore.login(userName, password);
    console.log(login);

    if (login) {
      loginName.value = userName;
      localStorage.setItem("loginName", userName);
    } else {
      messageStore.showMessage("Login หรือ Password ไม่ถูกต้อง");
    }
    return login;
  };

  const logout = () => {
    loginName.value = "";
    localStorage.removeItem("loginName");
  };
  const loadData = () => {
    loginName.value = localStorage.getItem("loginName") || "";
  };

  return { loginName, isLogin, login, logout, loadData };
});
