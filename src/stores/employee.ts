import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Employee from "@/type/employee";
import axios from "axios";

export const useEmployeeStore = defineStore("Employee", () => {
  const employee = ref<Employee[]>([]);
  async function getEmployee() {
    try {
      const res = await axios.get("http://localhost/test-router");
      console.log(res);
    } catch (e) {
      console.log(e);
    }
  }
  return { employee, getEmployee };
});
