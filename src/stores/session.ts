import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useSessionStore = defineStore("order", () => {
  const login = ref(false);

  //   const doubleCount = computed(() => count.value * 2);

  function loginAction() {
    login.value = true;
  }

  function logoutAction() {
    login.value = false;
  }

  return { login, loginAction, logoutAction };
});
