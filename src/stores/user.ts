import { ref } from "vue";
import { defineStore } from "pinia";
import type User from "@/type/User";
import userService from "@/services/user";
import type NewUser from "@/type/NewUser";
//import { VCardItem } from "vuetify/lib/components";

export const useUserStore = defineStore("user", () => {
  const dialog = ref(false);
  const isTable = ref(true);
  const users = ref<User[]>([]);
  const editedUser = ref<User>({
    id: -1,
    login: "",
    role: "",
    salary: "",
    name: "",
    password: "",
  });
  const getUsers = async () => {
    try {
      const res = await userService.getUsers();
      users.value = res.data;
    } catch (e) {
      console.log(e);
    }
  };
  const login = async (loginName: string, password: string) => {
    const res = await userService.loginUser(loginName, password);
    return res.data;
  };
  const deleteUser = async (id: number) => {
    try {
      const res = await userService.deleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
    }
  };
  const saveUser = async () => {
    try {
      if (editedUser.value.id == -1) {
        const newUser = ref<NewUser>({
          login: editedUser.value.login,
          role: editedUser.value.role,
          salary: editedUser.value.salary,
          name: editedUser.value.name,
          password: editedUser.value.password,
        });
        await userService.createUser(newUser.value);
      } else {
        await userService.updateUser(editedUser.value);
      }

      await getUsers();
    } catch (error) {
      console.log(error);
    }
  };
  const editUser = async (user: User) => {
    editedUser.value = { ...user };
    dialog.value = true;
  };
  const clear = () => {
    editedUser.value = { id: -1, login: "", name: "", password: "" };
  };

  return {
    clear,
    users,
    deleteUser,
    dialog,
    saveUser,
    editUser,
    isTable,
    login,
    getUsers,
    editedUser,
  };
});
