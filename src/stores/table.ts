import { ref, watch } from "vue";
import { defineStore } from "pinia";
import router from "@/router";
import type Table from "@/type/Table";
import tableService from "@/services/table";
import type { VForm } from "vuetify/lib/components";
import axios from "axios";

export const useTableStore = defineStore("table", () => {
  const dialog = ref(false);
  const table = ref<Table[]>([]);
  const form = ref<VForm | null>(null);

  const tables = ref<Table[]>([
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
  ]);
  const tableSelected = ref<number | null>(null);
  async function selectTable(index: number) {
    console.log(await tableService.getTable());
    console.log(index);
    tableSelected.value = index;
  }
  // const tab = tableSelected.value + 1;
  const UpdateConfirmtable = ref<Table>({
    id: 0,
    Table_Status: true,
  });
  const UpdateCanceltable = ref<Table>({
    id: 0,
    Table_Status: false,
  });
  async function getTable() {
    try {
      const res = await tableService.getTable();
      table.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }
  async function confirmBooking() {
    console.log(tableSelected.value);
    if (tableSelected.value !== null && tableSelected.value + 1 > 0) {
      tables.value[tableSelected.value].Table_Status = true;
      await tableService.createTable(
        (UpdateConfirmtable.value.id = tableSelected.value + 1),
        UpdateConfirmtable.value
      );
      tableSelected.value = null;
      console.log(tableSelected.value);
      window.location.reload();
    }
  }

  async function moveTable() {
    console.log(tableSelected.value);
    if (tables.value[tableSelected.value]?.Table_Status == null) {
      const newIndex = prompt(
        "Enter the number of the table you want to move to:"
      );
      // delete the current table from the database
      await tableService.deleteStock(
        (UpdateCanceltable.value.id = tableSelected.value + 1),
        UpdateCanceltable.value
      );
      if (
        newIndex !== null &&
        newIndex !== "" &&
        !isNaN(Number(newIndex)) &&
        Number(newIndex) >= 1 &&
        Number(newIndex) <= tables.value.length &&
        Number(newIndex) !== tableSelected.value! + 1
      ) {
        console.log(Number(newIndex) + 1);
        // update the Table_Status status of the tables
        tables.value[Number(newIndex) - 1].Table_Status = true;
        tables.value[tableSelected.value!].Table_Status = false;
        // create a new table in the database
        await tableService.createTable(
          (UpdateConfirmtable.value.id = Number(newIndex)),
          UpdateConfirmtable.value
        );

        tableSelected.value = null;
        window.location.reload();
      }
    }
  }
  async function cancelTable() {
    console.log(tableSelected.value);
    if (tables.value[tableSelected.value].Table_Status == null) {
      if (
        confirm(
          `Are you sure you want to cancel ${tableSelected.value + 1} table?`
        )
      ) {
        tables.value[tableSelected.value!].Table_Status = false;
        await tableService.deleteStock(
          (UpdateCanceltable.value.id = tableSelected.value + 1),
          UpdateCanceltable.value
        );
        tableSelected.value = null;
        window.location.reload();
      }
    }
  }

  function checkBill() {
    if (tables.value[tableSelected.value]?.Table_Status == null) {
      router.push("/bill");
    }
  }
  watch(dialog, (newDialog: any, oldDialog: any) => {
    console.log(newDialog);
    if (!newDialog) {
      UpdateConfirmtable.value = { Table_Status: "โต๊ะถูกจอง" };
    }
  });

  return {
    table,
    UpdateConfirmtable,
    UpdateCanceltable,
    tables,
    tableSelected,
    selectTable,
    confirmBooking,
    moveTable,
    cancelTable,
    checkBill,
    getTable,
  };
});
