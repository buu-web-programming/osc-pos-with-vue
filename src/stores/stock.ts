import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Stock from "@/type/Stock";
import stockService from "@/services/stock";

export const useStockStore = defineStore("Stock", () => {
  const dialog = ref(false);
  const stocks = ref<Stock[]>([]);
  const editedStock = ref<Stock>({ name: "", qty: 0, unit: "" });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedStock.value = { name: "", qty: 0, unit: "" };
    }
  });
  async function getStocks() {
    try {
      const res = await stockService.getStocks();
      stocks.value = res.data;
    } catch (e) {
      console.log(e);
    }
  }
  async function saveStocks() {
    try {
      if (editedStock.value.id) {
        const res = await stockService.updateStock(
          editedStock.value.id,
          editedStock.value
        );
      } else {
        const res = await stockService.saveStock(editedStock.value);
      }

      dialog.value = false;

      await getStocks();
    } catch (e) {
      console.log(e);
    }
  }
  async function deleteStock(id: number) {
    try {
      const res = await stockService.deleteStock(id);
      await getStocks();
    } catch (e) {
      console.log(e);
    }
  }
  function editStock(stock: Stock) {
    editedStock.value = stock;
    dialog.value = true;
  }

  return {
    stocks,
    getStocks,
    dialog,
    editedStock,
    saveStocks,
    editStock,
    deleteStock,
  };
});
