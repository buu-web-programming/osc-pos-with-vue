import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Menu from "@/type/Menu";
import menuService from "@/services/menu";
import axios from "axios";

export const useOrderStore = defineStore("order", () => {
  const functionChoose = ref(0);

  //   const doubleCount = computed(() => count.value * 2);

  const menu = ref<Menu[]>([]);

  const orderAdded = ref<Menu[]>([]);

  async function addMenu(menu: Menu, orderId: number) {
    await axios
      .post("http://localhost:3000/orderitem", {
        qty: 1,
        status: "waiting",
        menu_id: menu.Menu_id,
        order_id: orderId,
      })
      .catch((error) => {
        console.error(error);
      });
  }

  async function getMenu() {
    try {
      const res = await menuService.getMenu();
      menu.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
    }
  }

  function choose(funcIndex: number) {
    functionChoose.value = funcIndex;
  }

  return { choose, functionChoose, getMenu, menu, orderAdded, addMenu };
});
