import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import LoginViewVue from "@/views/LoginView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "login",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: LoginViewVue,
    },
    {
      path: "/home",
      name: "home",
      component: () => import("../views/OrderView.vue"),
    },

    {
      path: "/Router",
      name: "router",
      component: () => import("../views/RouterToView.vue"),
    },
    {
      path: "/order",
      name: "order",
      component: () => import("../views/OrderView.vue"),
    },

    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/welcome",
      name: "welcome",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/HomeView.vue"),
    },
    {
      path: "/table",
      name: "table",
      component: () => import("../views/TableBookingView.vue"),
    },
    {
      path: "/stock",
      name: "stock",
      component: () => import("../views/StockView.vue"),
    },
    {
      path: "/test-router",
      name: "router",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/UserAccountEmView.vue"),
    },
    {
      path: "/bill",
      name: "bill",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/BillView.vue"),
    },
    {
      path: "/statusorem",
      name: "statusorderemp",
      component: () => import("../views/StatusOrderEmView.vue"),
    },

    {
      path: "/served",
      name: "serve",
      component: () => import("../views/ServedView.vue"),
    },
    {
      path: "/user",
      name: "user",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/user/UserView.vue"),
    },
  ],
});

export default router;
