export default interface Employee {
  id: number;
  name: string;
  role: string;
  salary: number;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}
