export default interface Stock {
  id?: number;
  name: string;
  qty: number;
  unit: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
