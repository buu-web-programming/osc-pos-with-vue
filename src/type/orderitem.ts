export default interface Orderitem {
    id: number;
    Order_date: number;
    Order_total: number;
    Order_status: string;
  }