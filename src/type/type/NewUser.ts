export default interface NewUser {
  login: string;
  name: string;
  password: string;
}
