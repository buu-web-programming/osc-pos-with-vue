export default interface User {
  id: number;
  login: string;
  name: string;
  role: string;
  salary: number;
  password: string;
  createdAt?: Date;
  updatedAt?: Date;
}
