export default interface Menu {
  Menu_id: number;
  Menu_name: string;
  Menu_price: number;
  image: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
}
