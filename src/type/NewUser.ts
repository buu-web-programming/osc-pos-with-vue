export default interface NewUser {
  login: string;
  role: string;
  salary: number;
  name: string;
  password: string;
}
